# Wholesail
## Overview
This challenge problem is designed to reflect the work of Accounting Platform team
does on a day to day basis to import and normalize data from third party financial service
partners.

## QuickStart
### Project Structure
|-----JavaDoc # This folder contains generated JavaDoc.

|-----docker # Where the docker related files placed

|-----src  # Where the java code and test cases places

### Build Project
#### Prerequisites
1. Clone this repository
2. Java 19 (I am using OpenJDK19, if you are blocked by any issue, contact with me please ```lianglu_bjut@aliyun.com```)
3. Maven 3.5+
4. Docker - make sure you can run ```docker``` in your command line, [docker installation](https://docs.docker.com/docker-for-mac/install/).


### Run with Docker
##### Prerequisites
1. CD to the root folder of the cloned project. (Must)
2. ```mvn clean install -DskipTests=true```
3. ```docker-compose -f docker/docker-compose.yml up -d```

Note: service's port will be mapped on 8081.

### Run with Local Environment
1. Import to your IDE
2. ```mvn clean install```
3. Start the Spring boot application 'WholesailApplication'

## Play with UI
1. Open ```http://localhost:8081/GenerateReportByUploadFile.html``` on browser.
2. The metadata IDs should have been populated in the first dropdown list. Choose one to parse report.
3. Upload one invoice file by clicking 'Choose File'.
4. Click 'Generate Balance Report' button, the report will be displayed below.
5. **We can easily create a new mapping for another accounting system with an API (check Step#2 below), then come back to use it from UI.**

![UI_ScreenShot.png](UI_ScreenShot.png)

## Play with API
1. Import the [Mapping_Metadata_Managment_Docker.postman_collection.json](postmanCollection%2FMapping_Metadata_Managment_Docker.postman_collection.json) to Postman 
   (Please find the file in folder 'postmanCollection'))
2. Trigger the ```Create Mapping Meta for New Integration``` to create a new mapping for another seller's accounting system.  
   2.1. The current example payload ([GoldenGateMetadata.json](src%2Fmain%2Fresources%2FinvoiceMappingMetadata%2FGoldenGateMetadata.json)) in this API can also be used to parse 'Golden Gate' invoices. Have a try.  
   2.2. The payload for HappyFruits' is here [HappyFruitsMetadata.json](src%2Fmain%2Fresources%2FinvoiceMappingMetadata%2FHappyFruitsMetadata.json)
3. Trigger the ```Get Mapping Metadata by Id``` with the UUID just returned from above request, to check the just created mapping. 
4. Trigger the ```Get 'HappyFruits' Mapping Metadata``` to check Out-Of-The-Box HappyFruits' mapping metadata. 
5. Trigger the ```Get 'GoldenGate' Mapping Metadata``` to check Out-Of-The-Box GoldenGate's mapping metadata.
   

## Project status and roadmap
1. I plan to add a DB to persist all normalized invoices, invoice items and mapping metadata. 