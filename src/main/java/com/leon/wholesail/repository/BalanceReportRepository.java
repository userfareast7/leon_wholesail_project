package com.leon.wholesail.repository;

import com.leon.wholesail.model.BalanceReport;
import com.leon.wholesail.model.BalanceReportItem;
import org.springframework.stereotype.Repository;

import java.util.Map;
import java.util.Optional;

/**
 * The repository to persist balance report
 *
 * <p>This is code challenge project, I don't have time to implement a DB
 * So I use some container in memory to mimic DB. The report items are stored in report object as a map
 * Only because it is easier to search, just like DB index. Ideally I should use a List to store items.</p>
 *
 * <ul>
 *     <li>createBalanceReport: to mimic creating a report from DB</li>
 *     <li>addReportItemToReport: to mimic add/update a report item in DB</li>
 *     <li>getReportItemFromReport: to mimic query a item from DB</li>
 * </ul>
 */
@Repository
public class BalanceReportRepository {

    public BalanceReport createBalanceReport(String sellerId, String sellerName) {
        return new BalanceReport(sellerId, sellerName);
    }

    public void addReportItemToReport(BalanceReport report, String buyerId, BalanceReportItem item) {
        Map<String, BalanceReportItem> itemMap = report.getReportItemMap();
        if (!itemMap.containsKey(buyerId)) {
            itemMap.put(buyerId, item);
        }
    }

    public Optional<BalanceReportItem> getReportItemFromReport(BalanceReport report, String buyerId) {
        Map<String, BalanceReportItem> itemMap = report.getReportItemMap();
        if (itemMap.containsKey(buyerId)) {
            return Optional.of(itemMap.get(buyerId));
        } else {
            return Optional.empty();
        }
    }

}
