package com.leon.wholesail.repository;

import com.google.common.annotations.VisibleForTesting;
import com.google.gson.Gson;
import com.leon.wholesail.metadata.InvoiceMapping;
import com.leon.wholesail.VO.MetadataVO;
import com.leon.wholesail.service.impl.MetadataServiceImpl;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Repository;

import java.io.InputStreamReader;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;


/**
 * The repository to persist metadata
 *
 * <p>This is code challenge project, I don't have time to implement a DB
 * So I use some container in memory to mimic DB. The metadata items are stored in a ConcurrentHashMap</p>
 *
 * <p>This class leverage Spring's extend point to create two Out-Of-The-Box metadata.
 * It starts a new thread to generate metadata and store in container.
 * Thus it would affect the start time of the server.</p>
 *
 * <ul>
 *     <li>For Out-Of-The-Box metadata, the fixed ID is assigned to make the API testing more clear and easy.</li>
 *     <li>For dynamic create metadata, a random UUID is generated to mimic the DB unique ID.</li>
 * </ul>
 */
@Repository
public class MetadataRepository implements ApplicationListener {
    @Autowired
    private ApplicationContext applicationContext;
    public static final Logger LOGGER = LoggerFactory.getLogger(MetadataRepository.class);
    public static final String GOLDEN_GATE_METADATA_ID = "GoldenGate_MetaData_Id";
    public static final String HAPPY_FRUITS_METADATA_ID = "HappyFruits_MetaData_Id";

    private MetadataRepository() {
    }

    private static MetadataRepository repository = new MetadataRepository();
    private static Map<String, InvoiceMapping> companyDataMappingInfoMap = new ConcurrentHashMap<>();

    public static MetadataRepository getInstance() {
        return repository;
    }


    /**
     * Save invoice mapping meta to DB
     *
     * @param invoiceMapping
     * @return
     */
    public InvoiceMapping save(InvoiceMapping invoiceMapping) {
        if (StringUtils.isBlank(invoiceMapping.getId())) {
            invoiceMapping.setId(UUID.randomUUID().toString());
        }
        companyDataMappingInfoMap.put(invoiceMapping.getId(), invoiceMapping);
        return invoiceMapping;
    }

    public InvoiceMapping getById(String id) {
        return companyDataMappingInfoMap.get(id);
    }

    public List<String> getAllIds() {
        return companyDataMappingInfoMap.keySet().stream().toList();
    }

    /**
     * Prepare out-of-the-box metadata when server start up
     * Then we can use them from 'DB' to parse invoice.
     * @param event
     */
    @Override
    public void onApplicationEvent(ApplicationEvent event) {
        if (this.applicationContext != event.getSource()) {
            return;
        }
        if (event instanceof ContextRefreshedEvent) {
            new Thread(() -> {
                // initialize two out-of-box mappings
                try {
                    LOGGER.info("Load out-of-box mapping start");
                    prepareGoldenGateMapping();
                    prepareHappyFruitsMapping();
                    LOGGER.info("Load out-of-box mapping end");
                } catch (Exception exception) {
                    LOGGER.error("Could not load OOTB mapping when server booting.", exception);
                }
            }).start();
        }
    }

    /**
     * Parse the Out-Of-Box mapping for GoldenGate and store to 'DB'
     */
    @VisibleForTesting
    public void prepareGoldenGateMapping() {
        Resource goldenGateFile = new ClassPathResource("invoiceMappingMetadata/GoldenGateMetadata.json");
        InvoiceMapping invoiceMapping = parseJsonFile(goldenGateFile);
        invoiceMapping.setId(GOLDEN_GATE_METADATA_ID);
        this.save(invoiceMapping);
    }

    /**
     * Parse the Out-Of-Box mapping for HappyFruits and store to 'DB'
     */
    @VisibleForTesting
    public void prepareHappyFruitsMapping() {
        Resource happyFruitsFile = new ClassPathResource("invoiceMappingMetadata/HappyFruitsMetadata.json");
        InvoiceMapping invoiceMapping = parseJsonFile(happyFruitsFile);
        invoiceMapping.setId(HAPPY_FRUITS_METADATA_ID);
        this.save(invoiceMapping);
    }


    private InvoiceMapping parseJsonFile(Resource goldenGateFile) {
        InvoiceMapping invoiceMapping = null;
        try {
            InputStreamReader reader = new InputStreamReader(goldenGateFile.getInputStream());
            MetadataVO metadataVO = new Gson().fromJson(reader, MetadataVO.class);
            invoiceMapping = MetadataServiceImpl.convertToInvoiceMapping(metadataVO);
        } catch (Exception e) {
            LOGGER.error("Exception happened during parse JSON file to InvoiceMappingMetadata. ", e);
        }
        return invoiceMapping;
    }
}
