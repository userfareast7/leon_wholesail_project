package com.leon.wholesail.controller;

import com.leon.wholesail.VO.BalanceReportVO;
import com.leon.wholesail.constant.ResponseMessage;
import com.leon.wholesail.VO.ParseFileVO;
import com.leon.wholesail.service.BalanceReportService;
import com.leon.wholesail.utils.BalanceReportVOConverter;
import com.leon.wholesail.utils.ResponseBuilder;
import com.leon.wholesail.utils.ResponseCode;
import jakarta.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.text.ParseException;

import static com.google.common.base.Preconditions.checkArgument;

@RestController
@RequestMapping("/balanceReport")
public class ParseFileController {
    @Autowired
    public ParseFileController(BalanceReportService balanceReportService) {
        this.balanceReportService = balanceReportService;
    }

    private BalanceReportService balanceReportService;

    /**
     * Two critical parameters are required:
     * <ul>
     *     <li>Invoice files are pre-stored in project.
     *     So the file path is required for now, but when we support file uploading, this is not needed.</li>
     *     <li>Mapping MetaDate Id is required. There are two predefined mapping metadata.
     *     But if you want to add more, you can create new metadata and use it to parse invoice files</li>
     * </ul>
     *
     * @param parseFileVO with file path and mapping metadata id in it
     * @return The generated report
     */
    @PostMapping
    public Object generateReport(@RequestBody ParseFileVO parseFileVO) throws IOException, ParseException, NoSuchFieldException, IllegalAccessException {

        checkArgument(parseFileVO != null, "Request body is required.");
        checkArgument(parseFileVO.getFilePath() != null,
                "Path of preserved file is required, until file uploading is support.");
        checkArgument(parseFileVO.getMetadataId() != null, "Mapping MetaDate Id is required");

        Resource resource = new ClassPathResource(parseFileVO.getFilePath());
        BalanceReportVO vo = BalanceReportVOConverter.convertToVO(balanceReportService
                .generateBalanceReport(parseFileVO.getMetadataId(), resource.getInputStream()));

        return ResponseEntity.ok(new ResponseBuilder()
                .setCode(ResponseCode.SUCCESS)
                .setMessage(ResponseMessage.SUCCESS_MESSAGE)
                .setData(vo).build());
    }

    /**
     * Upload a file, parse it to generate a balance report
     * @param file the invoice file
     * @param metadataId the metadata used to parse the file
     * @param request
     * @return The generated balance report
     */
    @PostMapping("/metadataId/{metadataId}")
    public Object upload(@RequestParam("file") MultipartFile file, @PathVariable("metadataId") String metadataId,
                         HttpServletRequest request)
            throws IllegalStateException, IOException, ParseException, NoSuchFieldException, IllegalAccessException {
        checkArgument(!file.isEmpty(), "Please upload a CSV file.");
        checkArgument(file.getContentType().equals("text/csv"), "Please upload CSV format file.");
        BalanceReportVO vo = BalanceReportVOConverter.convertToVO(balanceReportService
                .generateBalanceReport(metadataId, file.getInputStream()));
        return ResponseEntity.ok(new ResponseBuilder()
                .setCode(ResponseCode.SUCCESS)
                .setMessage(ResponseMessage.SUCCESS_MESSAGE)
                .setData(vo).build());
    }
}
