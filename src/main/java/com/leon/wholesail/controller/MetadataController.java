package com.leon.wholesail.controller;

import com.google.common.collect.ImmutableMap;
import com.leon.wholesail.constant.ResponseMessage;
import com.leon.wholesail.VO.MetadataVO;
import com.leon.wholesail.metadata.InvoiceMapping;
import com.leon.wholesail.service.MetadataService;
import com.leon.wholesail.service.impl.MetadataServiceImpl;
import com.leon.wholesail.utils.ResponseBuilder;
import com.leon.wholesail.utils.ResponseCode;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/metadata")
public class MetadataController {

    private MetadataService metadataService = new MetadataServiceImpl();

    /**
     * This is used to create a new Mapping MetaDate to support any new integrations.
     * There are two predefined mapping metadata: For Happy Fruits and Golden Gate.
     * With the new metadata create here, we can easily add new support and use it to parse invoice files
     * from other accounting system.
     *
     * @param metadataVO The JSON format metadata definition
     * @return Mapping MetaDate ID will be returned
     */
    @PostMapping
    public ResponseEntity<Object> createMetadata(@RequestBody MetadataVO metadataVO) {
        InvoiceMapping invoiceMapping = metadataService.createMetadata(metadataVO);
        return ResponseEntity.ok(new ResponseBuilder()
                .setCode(ResponseCode.SUCCESS)
                .setMessage(ResponseMessage.SUCCESS_MESSAGE)
                .setData(ImmutableMap.of("id", invoiceMapping.getId())).build());
    }


    /**
     * The API to query mapping metadata.
     * There are two pre-defined metadata:
     * <ul>
     *     <li>Golden Gate: the id is GoldenGate_MetaData_Id</li>
     *     <li>Happy Fruits: the id is HappyFruits_MetaData_Id</li>
     * </ul>
     *
     * @param id The id of matadata
     * @return
     */
    @GetMapping(value = "/{id:.+}")
    public ResponseEntity<Object> getMetadata(@PathVariable("id") String id) {
        InvoiceMapping mappingInfo = metadataService.getMetadataById(id);
        if (mappingInfo == null) {
            return ResponseEntity.ok().body(new ResponseBuilder()
                    .setCode(ResponseCode.BAD_REQUEST)
                    .setMessage(String.format("No mapping metadata found with the id %s.", id))
                    .build());
        } else {
            return ResponseEntity.ok(new ResponseBuilder()
                    .setCode(ResponseCode.SUCCESS)
                    .setMessage(ResponseMessage.SUCCESS_MESSAGE)
                    .setData(mappingInfo).build());
        }
    }

    /**
     * Get Ids for all metadata, this is used by UI
     * @return The list of IDs
     */
    @GetMapping()
    public ResponseEntity<Object> getAllMetadataId() {
        List<String> idList = metadataService.getAllMetadataIds();
        return ResponseEntity.ok(new ResponseBuilder()
                .setCode(ResponseCode.SUCCESS)
                .setMessage(ResponseMessage.SUCCESS_MESSAGE)
                .setData(idList).build());
    }
}
