package com.leon.wholesail.enums;

public enum NoteType {
    /**
     * VOIDED: represent the amount shouldn't be calculated in outstanding amount.
     */
    VOIDED, OTHER
}
