package com.leon.wholesail.enums;

public enum AmountUnit {
    /**
     * Two unit for USD
     * TODO: Need to consider other currency. Using a table to store decimal places for each currency.
     */
    CENT, DOLLAR
}
