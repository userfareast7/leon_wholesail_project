package com.leon.wholesail.model;

import lombok.Data;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Model for balance report.
 * <p>A balance report is provided for seller. Each seller have one or multiple reports on specific day.</p>
 *
 * <ul>
 *     <li>sellerId: id of seller, should be auto-generated</li>
 *     <li>sellerName: name of seller, used to display on UI or report</li>
 *     <li>reportItemMap: contains all report item,
 *     for each item represent aggregation of multiple invoices of one buyer</li>
 * </ul>
 */
@Data
public class BalanceReport {
    public BalanceReport(String sellerId, String sellerName) {
        this.sellerId = sellerId;
        this.sellerName = sellerName;
        this.reportItemMap = new ConcurrentHashMap<>();
    }

    private String sellerId;

    private String sellerName;

    private Map<String, BalanceReportItem> reportItemMap;

}
