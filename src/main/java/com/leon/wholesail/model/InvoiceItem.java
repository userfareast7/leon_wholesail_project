package com.leon.wholesail.model;

import lombok.Data;

import java.math.BigDecimal;

/**
 * Model for invoice item
 * TODO: If we have DB, need to generate our own unique Id for each invoice item
 * TODO: If we have DB, need to store invoice Id in item level as a reference.
 * TODO: If we have DB, need to store seller id for each invoice item
 * <ul>
 *     <li>item: the name of product</li>
 *     <li>quantity: the amount of purchase</li>
 *     <li>unitPrice: the price of a unit</li>
 *     <li>totalAmount: Total amount of this invoice item</li>
 *     <li>description: description and comments</li>
 * </ul>
 */
@Data
public class InvoiceItem {
    private String item;

    private Integer quantity;

    private BigDecimal unitPrice;

    private BigDecimal totalAmount;

    private String description;

}
