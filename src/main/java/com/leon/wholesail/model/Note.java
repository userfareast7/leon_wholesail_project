package com.leon.wholesail.model;

import com.leon.wholesail.enums.NoteType;
import lombok.Data;

/**
 * The model of Note
 * TODO: Since I designed a JSON format metadata to define the mapping,
 * TODO: I need a little more time to support mapping a field to a nested object's field
 * <ul>
 *     <li>type: the type of Note, currently it could be VOIDED or OTHER. We could add more</li>
 *     <li>noteMessage: the plain message text retrieved from CSV</li>
 * </ul>
 */
@Data
public class Note {
    private NoteType type;

    private String noteMessage;

}
