package com.leon.wholesail.model;

import com.leon.wholesail.metadata.InvoiceMapping;
import lombok.Data;

import java.util.UUID;

/**
 * The model of Seller.
 * TODO: We need a model for seller.
 * TODO: But since I designed a JSON format metadata to define the mapping,
 * TODO: I need a little more time to support mapping a field to a nested object's field.
 *
 * <ul>
 *     <li>id: the UUID of seller in wholesail system</li>
 *     <li>sellerName: the seller name </li>
 *     <li>invoiceMapping: the invoice mapping metadata that used to parse this seller's CSV</li>
 * </ul>
 */
@Data
public class Seller {

    private UUID id;

    private String sellerName;

    private InvoiceMapping invoiceMapping;
}
