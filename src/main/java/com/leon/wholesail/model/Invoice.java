package com.leon.wholesail.model;

import com.google.gson.JsonObject;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * Model for Wholesail invoice
 * TODO: If we have DB, need to generate our own unique Id for each invoice
 * TODO: If we have DB, need to store seller id for each invoice
 * <ul>
 *     <li>invoiceId: id of invoice in seller's system.
 *     <li>customerId: The buyer's ID</li>
 *     <li>transactionAmount: the amount of the transaction</li>
 *     <li>createdDate: The date the invoice is created</li>
 *     <li>adjustmentAmount: Could be positive or negative </li>
 *     <li>paymentTerm: the days expected to be paid after invoice creation</li>
 *     <li>paidAmount: the amount that already has been paid</li>
 *     <li>note: VOIDED means no need to calculate into outstanding amount</li>
 * </ul>
 */
@Data
public class Invoice {

    private String invoiceId;

    private Buyer buyer;

    private BigDecimal transactionAmount;

    private Date createdDate;

    private BigDecimal adjustmentAmount;

    private Integer paymentTerm;

    private List<InvoiceItem> invoiceItems;

    private Date paidDate;

    private BigDecimal paidAmount;

    private String note;

}
