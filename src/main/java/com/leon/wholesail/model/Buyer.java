package com.leon.wholesail.model;

import lombok.Data;

import java.util.UUID;

/**
 * The model of buyer.
 * TODO: We need a model for buyer.
 * TODO: But since I designed a JSON format metadata to define the mapping,
 * TODO: I need a little more time to support mapping a field to a nested object's field
 *
 * <ul>
 *     <li>id: the UUID of buyer in wholesail system</li>
 *     <li>seller: a reference to seller, represent which seller this buyer connected with</li>
 *     <li>buyerName: the buyer name retrieved from CSV</li>
 *     <li>billingAddress: the billingAddress retrieved from CSV</li>
 *     <li>shippingAddress: the shippingAddress retrieved from CSV</li>
 * </ul>
 */
@Data
public class Buyer {
    private UUID id;

    private Seller seller;
    private String buyerName;

    private String billingAddress;

    private String shippingAddress;
}
