package com.leon.wholesail.model;

import lombok.Data;

import java.math.BigDecimal;

/**
 * Model for balance report item.*
 * <ul>
 *     <li>customer: id of buyer, the customer of seller</li>
 *     <li>OutstandingBalance: The amount that need to be paid</li>
 *     <li>PastDueBalance: the amount that should already been paid</li>
 * </ul>
 */
@Data
public class BalanceReportItem {

    public BalanceReportItem(String customer) {
        this.customer = customer;
        this.OutstandingBalance = BigDecimal.ZERO;
        this.PastDueBalance = BigDecimal.ZERO;
    }

    private String customer;

    private BigDecimal OutstandingBalance;

    private BigDecimal PastDueBalance;
}
