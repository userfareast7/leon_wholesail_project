package com.leon.wholesail.metadata;

import com.leon.wholesail.enums.FieldType;
import lombok.Data;

/**
 * Detailed field level mapping metadata, with which we can map each data fields in seller's CSV
 * to Wholesail system.
 * <ul>
 *     <li>externalFieldName: represent the field name in seller's CSV file</li>
 *     <li>fieldType: the type of data, could be one of {@link FieldType}</li>
 *     <li>fieldPosition: The offsite of the field in CSV file</li>
 *     <li>internalFieldName: To which field of wholesail should seller's data be mapped</li>
 *     <li>nested: represent this field is a nested field</li>
 *     <li>nestedFieldName: represent which nested field the value should be mapped to</li>
 * </ul>
 */
@Data
public class InvoiceFieldMapping {

    private String externalFieldName;
    private FieldType fieldType;
    private int fieldPosition;
    private String internalFieldName;

    private Boolean nested;
    private String nestedFieldName;
    private InvoiceItemMapping invoiceItemMapping;
}
