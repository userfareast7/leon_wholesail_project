package com.leon.wholesail.metadata;

import lombok.Data;

import java.util.List;

/**
 * The meta for invoice item mapping.
 * One invoice could have multiple invoice items.
 * The detailed field mapping of invoice item is defined in {@link InvoiceItemFieldMapping}
 */
@Data
public class InvoiceItemMapping {

    private String entryName;

    private List<InvoiceItemFieldMapping> invoiceItemFieldMappingList;

}
