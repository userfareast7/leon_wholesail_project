package com.leon.wholesail.metadata;

import com.leon.wholesail.enums.AmountUnit;
import lombok.Data;

import java.util.Map;

/**
 * Mapping metadata that used to map data from seller's system to wholesail system.
 * <p>The {@code mappingFields} contains detailed fields mapping</p>
 *
 *  <ul>
 *      <li>id: the identifier of this invoice mapping meta, generated UUID.</li>
 *      <li>accountingSystemName: The name of seller accounting system</li>
 *      <li>dateFormat: The date format that used in CSV file</li>
 *      <li>amountUnit: The amount unit that used in CSV file. Could be one of {@link AmountUnit}</li>
 *  </ul>
 */
@Data
public class InvoiceMapping {
    private String id;
    private String accountingSystemName;
    private String dateFormat;
    private AmountUnit amountUnit;
    private Map<String, InvoiceFieldMapping> mappingFields;

    public InvoiceMapping(String id, String accountingSystemName, String dateFormat, AmountUnit amountUnit, Map<String, InvoiceFieldMapping> mappingFields) {
        this.id = id;
        this.accountingSystemName = accountingSystemName;
        this.dateFormat = dateFormat;
        this.amountUnit = amountUnit;
        this.mappingFields = mappingFields;
    }

    public InvoiceMapping(String accountingSystemName, String dateFormat, AmountUnit amountUnit, Map<String, InvoiceFieldMapping> mappingFields) {
        this.accountingSystemName = accountingSystemName;
        this.dateFormat = dateFormat;
        this.amountUnit = amountUnit;
        this.mappingFields = mappingFields;
    }
}

