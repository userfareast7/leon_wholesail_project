package com.leon.wholesail.metadata;

import com.leon.wholesail.enums.FieldType;
import lombok.Data;

/**
 * invoice item field mapping metadata, with which we can map each invoice item's field
 * in seller's CSV to Wholesail system.
 * <ul>
 *     <li>externalFieldName: represent the invoice item field name in seller's CSV file</li>
 *     <li>fieldType: the type of data, could be one of {@link FieldType}</li>
 *     <li>internalFieldName: To which invoice item field of wholesail should seller's data be mapped</li>
 * </ul>
 */
@Data
public class InvoiceItemFieldMapping {
    private String externalFieldName;
    private FieldType fieldType;
    private String internalFieldName;
}
