package com.leon.wholesail.constant;

public class ResponseMessage {
    public static final String SUCCESS_MESSAGE = "Success";

    public static final String BAD_REQUEST_MESSAGE = "Invalid Request. Please adjust and try again.";

    public static final String INTERNAL_ERROR_MESSAGE = "The service is not available right now due to" +
            " some internal issue. Please try later or contact with our support engineer.";
}
