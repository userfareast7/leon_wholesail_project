package com.leon.wholesail.utils;

import com.google.common.base.Charsets;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.leon.wholesail.enums.AmountUnit;
import com.leon.wholesail.enums.FieldType;
import com.leon.wholesail.metadata.InvoiceFieldMapping;
import com.leon.wholesail.metadata.InvoiceItemFieldMapping;
import com.leon.wholesail.metadata.InvoiceItemMapping;
import com.leon.wholesail.metadata.InvoiceMapping;
import com.leon.wholesail.model.Invoice;
import com.leon.wholesail.model.InvoiceItem;
import org.apache.commons.csv.CSVRecord;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.util.ReflectionUtils;

import java.io.*;
import java.lang.reflect.Field;
import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Utility class to parse invoice file to wholesail invoice model with specific mapping meta.
 */
@Component
public class InvoiceParseUtil {

    private static final Logger LOGGER = LoggerFactory.getLogger(InvoiceParseUtil.class);

    /**
     * This method leverage {@link CSVUtil} to parse CSV files.
     * Then map the values of each fields to invoice object based on the mapping metadata
     * <ul>
     *     <li>For date type: we allow different date formats be defined in mapping metadata. {@see MetadataRequest}</li>
     *     <li>For decimal type: since it is used to represent USD amount, there are two types of units defined in {@link AmountUnit}</li>
     *     <li>For JSON type: it will be mapped to a JSONObject. The same type should be assigned in invoice.</li>
     *     <li>For LIST_IN_JSON: it is designed to handle JSONArray, for example the invoice items in CSV</li>
     * </ul>
     *
     *
     * <p>TODO: Need to consider some fields are not amount related, but also use float or decimal type.</p>
     * <p>TODO: Need to consider how to handle other currency</p>
     *
     * @param inputStream
     * @param invoiceMapping
     * @return
     * @throws IllegalAccessException
     * @throws ParseException
     */
    public List<Invoice> parseData(InputStream inputStream, InvoiceMapping invoiceMapping) throws IllegalAccessException, ParseException {
        Map<String, InvoiceFieldMapping> mappings = invoiceMapping.getMappingFields();
        List<Invoice> list = new ArrayList<>();
        try (Reader in = new BufferedReader(new InputStreamReader(inputStream, Charsets.UTF_8))) {
            // The first line is the header line, skip it
            List<CSVRecord> records = CSVUtil.getCsvRecords(in);
            for (int i = 1; i < records.size(); i++) {
                Iterator<String> it = mappings.keySet().iterator();
                Invoice invoice = new Invoice();
                while (it.hasNext()) {
                    String fieldName = it.next();
                    InvoiceFieldMapping mapping = mappings.get(fieldName);
                    if (mapping.getFieldType() == FieldType.STRING) {
                        setInvoiceField(invoice, mapping, getStringValue(records.get(i), mapping));
                    } else if (mapping.getFieldType() == FieldType.DATE) {
                        setInvoiceField(invoice, mapping, getDateValue(records.get(i), mapping, invoiceMapping.getDateFormat()));
                    } else if (mapping.getFieldType() == FieldType.INTEGER) {
                        setInvoiceField(invoice, mapping, getIntegerValue(records.get(i), mapping));
                    } else if (mapping.getFieldType() == FieldType.JSON) {
                        setInvoiceField(invoice, mapping, getJsonValue(records.get(i), mapping, JsonObject.class));
                    } else if (mapping.getFieldType() == FieldType.DECIMAL) {
                        setInvoiceField(invoice, mapping, getDecimalValue(records.get(i), mapping, invoiceMapping.getAmountUnit()));
                    } else if (mapping.getFieldType() == FieldType.LIST_IN_JSON) {
                        setInvoiceField(invoice, mapping, getListInJsonValue(records.get(i), mapping));
                    }
                }
                list.add(invoice);
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        return list;
    }

    private String getStringValue(CSVRecord csvRecord, InvoiceFieldMapping fieldMetadata) {
        return csvRecord.get(fieldMetadata.getFieldPosition());
    }

    private Integer getIntegerValue(CSVRecord csvRecord, InvoiceFieldMapping fieldMetadata) {
        String value = csvRecord.get(fieldMetadata.getFieldPosition());
        if (StringUtils.isBlank(value)) {
            return Integer.valueOf(0);
        }
        return Integer.parseInt(value);
    }

    private Date getDateValue(CSVRecord csvRecord, InvoiceFieldMapping fieldMetadata, String dateFormat) throws ParseException {
        String value = csvRecord.get(fieldMetadata.getFieldPosition());
        if (StringUtils.isBlank(value)) {
            return null;
        }
        return new SimpleDateFormat(dateFormat).parse(value);
    }

    private BigDecimal getDecimalValue(CSVRecord csvRecord, InvoiceFieldMapping fieldMetadata, AmountUnit amountUnit) {
        String value = csvRecord.get(fieldMetadata.getFieldPosition());
        if (StringUtils.isBlank(value)) {
            return BigDecimal.ZERO;
        }
        BigDecimal bigDecimalValue = new BigDecimal(value);
        return amountUnit == AmountUnit.CENT ? bigDecimalValue.divide(BigDecimal.valueOf(100)) : bigDecimalValue;
    }

    private <T> T getJsonValue(CSVRecord csvRecord, InvoiceFieldMapping fieldMetadata, Class<T> clazz) {
        return new Gson().fromJson(csvRecord.get(fieldMetadata.getFieldPosition()), clazz);
    }

    private List<InvoiceItem> getListInJsonValue(CSVRecord csvRecord, InvoiceFieldMapping fieldMetadata) {
        JsonObject jsonObject = new Gson().fromJson(csvRecord.get(fieldMetadata.getFieldPosition()), JsonObject.class);
        InvoiceItemMapping invoiceItemMapping = fieldMetadata.getInvoiceItemMapping();
        String entryName = invoiceItemMapping.getEntryName();
        JsonArray itemFields = jsonObject.getAsJsonArray(entryName);
        List<InvoiceItemFieldMapping> itemFieldMappings = invoiceItemMapping.getInvoiceItemFieldMappingList();
        List<InvoiceItem> list = extractInvoiceItems(itemFields, itemFieldMappings);
        return list;
    }

    /**
     * This method parse each invoice item as JSON, then populate the item field values to invoice item based on
     * mapping defined in {@link InvoiceItemFieldMapping}.
     *
     * @param itemFields        The JsonArray extracted from CSV, contains all items of this invoice
     * @param itemFieldMappings The pre-defined invoice item field mapping metadata.
     * @return
     */
    private List<InvoiceItem> extractInvoiceItems(JsonArray itemFields, List<InvoiceItemFieldMapping> itemFieldMappings) {
        try {
            List<InvoiceItem> invoiceItemList = new ArrayList<>();
            for (JsonElement itemFieldElement : itemFields) {
                JsonObject itemField = itemFieldElement.getAsJsonObject();
                InvoiceItem invoiceItem = new InvoiceItem();
                Iterator<InvoiceItemFieldMapping> it = itemFieldMappings.iterator();
                while (it.hasNext()) {
                    InvoiceItemFieldMapping mapping = it.next();
                    if (FieldType.STRING.equals(mapping.getFieldType())) {
                        setInvoiceItemField(invoiceItem, mapping, itemField.get(mapping.getExternalFieldName()).getAsString());
                    } else if (FieldType.INTEGER.equals(mapping.getFieldType())) {
                        setInvoiceItemField(invoiceItem, mapping, itemField.get(mapping.getExternalFieldName()).getAsInt());
                    } else if (FieldType.DECIMAL.equals(mapping.getFieldType())) {
                        setInvoiceItemField(invoiceItem, mapping, itemField.get(mapping.getExternalFieldName()).getAsBigDecimal());
                    }
                }
                invoiceItemList.add(invoiceItem);
            }
            return invoiceItemList;
        } catch (Exception e) {
            LOGGER.error("Unexpected exception happened when parsing invoice item", e);
            // TODO: return null for temp, this need to be handled in better way.
            return null;
        }
    }

    /**
     * This function help to set field value to invoice.
     * It can handle top level field and second level field.
     * The second level field means the buyerName field in buyer. The buyer is the top level object.
     * <p>
     * <ul>
     *     Second level fields are:
     *     <li>buyerName</li>
     *     <li>shippingAddress</li>
     *     <li>billingAddress</li>
     * </ul>
     * </p>
     * <p>
     * The metadata for second level fields looks like:
     *     <p>
     *    {
     *       "externalFieldName": "billing_address",
     *       "fieldType": "STRING",
     *       "fieldPosition": 7,
     *       "nested": true,
     *       "nestedFieldName": "billingAddress",
     *       "internalFieldName": "buyer"
     *     }
     *
     *     <ul>
     *      <li>The {@code nested} represent this field is a nested field</li>
     *      <li>The {@code nestedFieldName} represent which field the value should be mapped to</li>
     *      <li>The {@code internalFieldName} represent which top-level object the nested field belong to</li>
     *     </ul>
     * </p>
     *
     * @param obj     the target invoice that will be set with value
     * @param mapping the invoice field level mapping
     * @param value   the value to be set to invoice
     * @throws IllegalAccessException
     */
    private static void setInvoiceField(Object obj, InvoiceFieldMapping mapping, Object value) {
        String internalFieldName = mapping.getInternalFieldName();
        if (mapping.getNested() == null || !mapping.getNested()) {
            Field targetField = ReflectionUtils.findField(Invoice.class, internalFieldName);
            ReflectionUtils.makeAccessible(targetField);
            if (value != null) {
                try {
                    targetField.set(obj, value);
                } catch (IllegalAccessException e) {
                    LOGGER.error(String.format("Exception happened when populate nested field %s"
                            , internalFieldName), e);
                }
            }
        } else {
            // Handle nested field
            String nestedFieldName = mapping.getNestedFieldName();
            Field targetParentField = ReflectionUtils.findField(Invoice.class, internalFieldName);
            ReflectionUtils.makeAccessible(targetParentField);
            Object nestedObject = ReflectionUtils.getField(targetParentField, obj);
            if (nestedObject == null) {
                // Initialize nested Object
                try {
                    nestedObject = Class.forName(targetParentField.getType().getName()).newInstance();
                    ReflectionUtils.setField(targetParentField, obj, nestedObject);
                } catch (ClassNotFoundException | InstantiationException | IllegalAccessException exception) {
                    LOGGER.error("Exception happened when parse nested field " + nestedFieldName, exception);
                    return;
                }
            }
            Field targetNestField = ReflectionUtils.findField(nestedObject.getClass(), nestedFieldName);
            ReflectionUtils.makeAccessible(targetNestField);
            if (value != null) {
                try {
                    targetNestField.set(nestedObject, value);
                } catch (IllegalAccessException e) {
                    LOGGER.error(String.format("Exception happened when populate nested field %s"
                            , internalFieldName), e);
                }
            }
        }

    }

    private static void setInvoiceItemField(Object obj, InvoiceItemFieldMapping mapping, Object value) {
        Field targetField = ReflectionUtils.findField(InvoiceItem.class, mapping.getInternalFieldName());
        ReflectionUtils.makeAccessible(targetField);
        if (value != null) {
            try {
                targetField.set(obj, value);
            } catch (IllegalAccessException e) {
                LOGGER.error(String.format("Exception happened when populate nested field %s"
                        , mapping.getExternalFieldName()), e);
            }
        }
    }
}
