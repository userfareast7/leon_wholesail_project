package com.leon.wholesail.utils;

/**
 * Wholesail defined API response codes
 */
public enum ResponseCode {
    SUCCESS(10200), BAD_REQUEST(10400), INTERNAL_ERROR(10500);
    private final int value;

    ResponseCode(final int value) {
        this.value = value;
    }

    public int getValue() {
        return this.value;
    }
}
