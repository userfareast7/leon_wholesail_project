package com.leon.wholesail.utils;

import org.apache.commons.lang3.StringUtils;

import java.util.HashMap;
import java.util.Map;

/**
 * Common utility that help to organize API response code, message and data
 */
public class ResponseBuilder {

    public static final String KEY_MESSAGE = "message";

    public static final String KEY_DETAIL = "detail";
    public static final String KEY_CODE = "code";
    public static final String KEY_DATA = "data";

    private String message;

    private String detail;
    private ResponseCode code;
    private Object data;

    public ResponseBuilder setDetail(String detail) {
        this.detail = detail;
        return this;
    }

    public ResponseBuilder setMessage(String message) {
        this.message = message;
        return this;
    }

    public ResponseBuilder setCode(ResponseCode code) {
        this.code = code;
        return this;
    }

    public ResponseBuilder setData(Object data) {
        this.data = data;
        return this;
    }

    public Map<String, Object> build() {
        Map<String, Object> response = new HashMap<>();
        response.put(KEY_CODE, this.code.getValue());
        response.put(KEY_MESSAGE, this.message);
        if (!StringUtils.isBlank(this.detail)) {
            response.put(KEY_DETAIL, this.detail);
        }
        if (this.data != null) {
            response.put(KEY_DATA, this.data);
        }
        return response;
    }
}
