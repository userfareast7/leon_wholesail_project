package com.leon.wholesail.utils;

import com.leon.wholesail.VO.BalanceReportVO;
import com.leon.wholesail.model.BalanceReport;

import static com.google.common.base.Preconditions.checkArgument;

public class BalanceReportVOConverter {

    /**
     * This class is required only because we temporarily use a Map to store invoiceItem in BalanceReport
     * The reason of that is to mimic searching item of a report item in DB.
     * Ideally I should use a list as container.
     * When returning data to API caller, the items need to be stored in a list.
     * That's why this converter is needed temporarily.
     * If we have database, this can be removed.
     *
     * @param balanceReport The report to be converted
     * @return The report VO
     */
    public static BalanceReportVO convertToVO(BalanceReport balanceReport) {
        checkArgument(balanceReport != null, "balanceReport is required.");
        BalanceReportVO vo = new BalanceReportVO(balanceReport.getSellerId(), balanceReport.getSellerName());
        vo.setReportItemList(balanceReport.getReportItemMap().values()
                .stream().sorted((x, y) -> x.getCustomer().compareTo(y.getCustomer())).toList());
        return vo;
    }
}
