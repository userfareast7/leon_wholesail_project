package com.leon.wholesail.utils;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;

import java.io.IOException;
import java.io.Reader;
import java.util.List;


/**
 * <p>Class <tt>CsvUtils</tt> used to help to populate and read the CSV content
 */
public class CSVUtil {

    /**
     * Get CSV records
     *
     * @return {@code CSVRecord} collection
     */
    public static List<CSVRecord> getCsvRecords(Reader in) throws IOException {
        CSVParser csvFileParser = new CSVParser(in, CSVFormat.RFC4180);
        return csvFileParser.getRecords();
    }
}
