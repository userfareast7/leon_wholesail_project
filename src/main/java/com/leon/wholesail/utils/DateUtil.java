package com.leon.wholesail.utils;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;

public class DateUtil {
    /**
     * LocalDateTime is more recommended since it is thread safe and others.
     * If a date object is use, convert it to LocalDateTime.
     *
     * @param date the date to be converted
     * @return an instance of LocalDateTime
     */
    public static LocalDateTime asLocalDateTime(Date date) {
        return Instant.ofEpochMilli(date.getTime()).atZone(ZoneId.systemDefault()).toLocalDateTime();
    }
}
