package com.leon.wholesail.service;

import com.leon.wholesail.model.BalanceReport;

import java.io.InputStream;
import java.text.ParseException;

public interface BalanceReportService {
    /**
     * The critical calculation logic:
     * <ul>
     *     <li>Past due: InvoiceDate + Payment term < currentDate</li>
     *     <li>When invoice past due: PastDueBalance = outstandingBalance; Otherwise, it's zero.</li>
     *     <li>The OutstandingBalance = TransactionAmount + (+/-) Adjustment + (-) PaidAmount</li>
     * </ul>
     *
     * @param metadataId  the id of metadata used to parse
     * @param inputStream the input stream of file
     * @return generated BalanceReport
     */
    BalanceReport generateBalanceReport(String metadataId, InputStream inputStream) throws ParseException, NoSuchFieldException, IllegalAccessException;
}
