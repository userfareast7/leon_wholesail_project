package com.leon.wholesail.service.impl;

import com.leon.wholesail.enums.AmountUnit;
import com.leon.wholesail.metadata.InvoiceFieldMapping;
import com.leon.wholesail.metadata.InvoiceMapping;
import com.leon.wholesail.VO.MetadataVO;
import com.leon.wholesail.repository.MetadataRepository;
import com.leon.wholesail.service.MetadataService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * The service to create metadata
 */
@Service
public class MetadataServiceImpl implements MetadataService {
    @Autowired
    private MetadataRepository metadataRepository = MetadataRepository.getInstance();

    @Override
    public InvoiceMapping createMetadata(MetadataVO metadataVO) {
        InvoiceMapping invoiceMapping = convertToInvoiceMapping(metadataVO);
        return metadataRepository.save(invoiceMapping);
    }

    @Override
    public InvoiceMapping getMetadataById(String id) {
        return metadataRepository.getById(id);
    }

    public List<String> getAllMetadataIds() {
        return metadataRepository.getAllIds();
    }

    public void setMetadataRepository(MetadataRepository metadataRepository) {
        this.metadataRepository = metadataRepository;
    }

    /**
     * Convert MetadataVO to InvoiceMapping
     * @param metadataVO the VO get from API
     * @return
     */
    public static InvoiceMapping convertToInvoiceMapping(MetadataVO metadataVO) {
        List<InvoiceFieldMapping> invoiceFieldMappingList = metadataVO.getMappingFields();
        Map<String, InvoiceFieldMapping> mappingFieldMetadataMap = new HashMap<>();
        invoiceFieldMappingList.forEach(metadata -> mappingFieldMetadataMap.put(metadata.getExternalFieldName(), metadata));
        InvoiceMapping invoiceMapping = new InvoiceMapping(metadataVO.getAccountingSystemName(),
                metadataVO.getDateFormat(), AmountUnit.valueOf(metadataVO.getAmountUnit()),
                mappingFieldMetadataMap);
        return invoiceMapping;
    }
}
