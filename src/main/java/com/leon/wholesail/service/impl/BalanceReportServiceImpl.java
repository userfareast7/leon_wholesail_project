package com.leon.wholesail.service.impl;


import com.google.common.annotations.VisibleForTesting;
import com.leon.wholesail.enums.NoteType;
import com.leon.wholesail.metadata.InvoiceMapping;
import com.leon.wholesail.model.BalanceReport;
import com.leon.wholesail.model.BalanceReportItem;
import com.leon.wholesail.model.Buyer;
import com.leon.wholesail.model.Invoice;
import com.leon.wholesail.repository.BalanceReportRepository;
import com.leon.wholesail.service.BalanceReportService;
import com.leon.wholesail.service.MetadataService;
import com.leon.wholesail.utils.DateUtil;
import com.leon.wholesail.utils.InvoiceParseUtil;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.InputStream;
import java.math.BigDecimal;
import java.text.ParseException;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import static com.google.common.base.Preconditions.checkArgument;

/**
 * The service class that consume invoices and generate balance report.
 */
@Service
public class BalanceReportServiceImpl implements BalanceReportService {
    public static final LocalDateTime CURRENT_DATE = LocalDateTime.of(2022, 3, 31,
            0, 0, 0);

    public static final Logger LOGGER = LoggerFactory.getLogger(BalanceReportServiceImpl.class);

    @Autowired
    private BalanceReportRepository balanceReportRepository;

    @Autowired
    private MetadataService metadataService;

    @Autowired
    private InvoiceParseUtil invoiceParseUtil;

    /**
     * The critical calculation logic:
     * <ul>
     *     <li>Past due: InvoiceDate + Payment term < currentDate</li>
     *     <li>When invoice past due: PastDueBalance = outstandingBalance; Otherwise, it's zero.</li>
     *     <li>The OutstandingBalance = TransactionAmount + (+/-) Adjustment + (-) PaidAmount</li>
     * </ul>
     *
     * @param metadataId  the id of metadata used to parse
     * @param inputStream the input stream of file
     * @return generated BalanceReport
     */
    @Override
    public BalanceReport generateBalanceReport(String metadataId, InputStream inputStream) throws ParseException, NoSuchFieldException, IllegalAccessException {

        InvoiceMapping invoiceMapping = metadataService.getMetadataById(metadataId);
        checkArgument(invoiceMapping != null);
        List<Invoice> list = invoiceParseUtil.parseData(inputStream, invoiceMapping);
        return this.generateBalanceReport(invoiceMapping.getId(), invoiceMapping.getAccountingSystemName(), list);
    }

    @VisibleForTesting
    public BalanceReport generateBalanceReport(String sellerId, String sellerName, List<Invoice> invoiceList) {
        BalanceReport balanceReport = balanceReportRepository.createBalanceReport(sellerId, sellerName);
        this.appendInvoiceList(balanceReport, invoiceList);
        return balanceReport;
    }

    private void appendInvoiceList(BalanceReport balanceReport, List<Invoice> invoiceList) {
        if (invoiceList != null && !invoiceList.isEmpty()) {
            for (Invoice invoice : invoiceList) {
                appendInvoice(balanceReport, invoice);
            }
        }
    }

    private void appendInvoice(BalanceReport balanceReport, Invoice invoice) {
        Buyer buyer = invoice.getBuyer();
        Optional<BalanceReportItem> itemOptional = balanceReportRepository.getReportItemFromReport(balanceReport, buyer.getBuyerName());
        BalanceReportItem balanceReportItem;
        if (!itemOptional.isPresent()) {
            balanceReportItem = new BalanceReportItem(invoice.getBuyer().getBuyerName());
        } else {
            balanceReportItem = itemOptional.get();
        }
        calculateOutstandingBalance(invoice, balanceReportItem);
        calculatePastDueBalance(invoice, balanceReportItem);

        if (!itemOptional.isPresent()) {
            balanceReportRepository.addReportItemToReport(balanceReport, buyer.getBuyerName(), balanceReportItem);
        }
        LOGGER.info(String.format("Finished handling invoice %s.", invoice.getInvoiceId()));
    }


    /**
     * Calculate if the invoice date pass due date
     *
     * @param date         the date invoice created
     * @param termSchedule the days expect the invoice is paid after creation
     * @param currentDate
     * @return ture if the date already past due
     */
    @VisibleForTesting
    public boolean hasPastDue(Date date, Integer termSchedule, LocalDateTime currentDate) {
        Date due = DateUtils.addDays(date, termSchedule);
        LocalDateTime dueDate = DateUtil.asLocalDateTime(due);
        return dueDate.isBefore(currentDate);
    }


    /**
     * Calculate the Past due balance.
     * When invoice past due, the outstanding amount will be the past due balance. Otherwise, it's zero.
     *
     * @param invoice
     * @param balanceReportItem
     */
    @VisibleForTesting
    public void calculatePastDueBalance(Invoice invoice, BalanceReportItem balanceReportItem) {
        if (!StringUtils.isBlank(invoice.getNote()) && NoteType.VOIDED.name().equals(invoice.getNote())) {
            LOGGER.info(String.format("Invoice %s PastDue Balance calculation is skipped since it is VOIDED", invoice.getInvoiceId()));
            return;
        }
        if (hasPastDue(invoice.getCreatedDate(), invoice.getPaymentTerm(), CURRENT_DATE)) {
            BigDecimal appendBalance = invoice.getTransactionAmount().add(invoice.getAdjustmentAmount()).subtract(invoice.getPaidAmount());
            BigDecimal newPastDueBalance = balanceReportItem.getPastDueBalance().add(appendBalance);
            balanceReportItem.setPastDueBalance(newPastDueBalance);
        }
    }

    /**
     * The OutstandingBalance = TransactionAmount + (+/-) Adjustment + (-) PaidAmount
     *
     * @param invoice
     * @param balanceReportItem
     */
    @VisibleForTesting
    public void calculateOutstandingBalance(Invoice invoice, BalanceReportItem balanceReportItem) {
        if (!StringUtils.isBlank(invoice.getNote()) && NoteType.VOIDED.name().equals(invoice.getNote())) {
            LOGGER.info(String.format("Invoice %s Outstanding Balance calculation is skipped since it is VOIDED", invoice.getInvoiceId()));
            return;
        }
        BigDecimal appendBalance = invoice.getTransactionAmount().add(invoice.getAdjustmentAmount()).subtract(invoice.getPaidAmount());
        BigDecimal newBalance = balanceReportItem.getOutstandingBalance().add(appendBalance);
        if (newBalance.compareTo(BigDecimal.ZERO) > 0) {
            balanceReportItem.setOutstandingBalance(newBalance);
        }
    }


    public void setBalanceReportRepository(BalanceReportRepository balanceReportRepository) {
        this.balanceReportRepository = balanceReportRepository;
    }
}
