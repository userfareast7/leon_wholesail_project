package com.leon.wholesail.service;

import com.leon.wholesail.VO.MetadataVO;
import com.leon.wholesail.metadata.InvoiceMapping;

import java.util.List;

public interface MetadataService {

    /**
     * Create metadata
     *
     * @param metadataVO
     * @return created mapping metadata
     */
    InvoiceMapping createMetadata(MetadataVO metadataVO);

    /**
     * Query mapping metadata
     *
     * @param id identifier of metadata
     * @return mapping metadata
     */
    InvoiceMapping getMetadataById(String id);

    List<String> getAllMetadataIds();
}
