package com.leon.wholesail.exception;


import com.leon.wholesail.constant.ResponseMessage;
import com.leon.wholesail.utils.ResponseBuilder;
import com.leon.wholesail.utils.ResponseCode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.util.Map;

/**
 * The common exception handler used to handle exceptions thrown from controllers
 * <p>
 * The exception {@code IllegalArgumentException} is handled separately, as {@code HttpStatus.BAD_REQUEST}
 * All other exceptions will be handled as {@code HttpStatus.INTERNAL_SERVER_ERROR}
 * </p>
 */
@RestControllerAdvice
public class GlobalDefaultExceptionHandler extends ResponseEntityExceptionHandler {
    private static final Logger LOGGER = LoggerFactory.getLogger(GlobalDefaultExceptionHandler.class);

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ResponseBody
    @ExceptionHandler(IllegalArgumentException.class)
    public Map<String, Object> handleIllegalArgumentException(IllegalArgumentException ex) {
        return new ResponseBuilder()
                .setCode(ResponseCode.BAD_REQUEST)
                .setMessage(ResponseMessage.BAD_REQUEST_MESSAGE)
                .setDetail(ex.getMessage())
                .build();
    }

    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    @ResponseBody
    @ExceptionHandler(Exception.class)
    public Map<String, Object> handleBaseException(Exception ex) {
        LOGGER.error("Unexpected exception happened: " + ex.getMessage());
        return new ResponseBuilder().setCode(ResponseCode.INTERNAL_ERROR)
                .setMessage(ResponseMessage.INTERNAL_ERROR_MESSAGE)
                .build();
    }
}
