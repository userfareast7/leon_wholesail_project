package com.leon.wholesail.VO;

import lombok.Data;

/**
 * <p>The input data structure to parse a file</p>
 *
 * <ul>
 *     <li>metadataId: to use which metadata mapping to parse the file</li>
 *     <li>filePath: The path of file to be parsed</li>
 * </ul>
 */
@Data
public class ParseFileVO {
    private String metadataId;
    private String filePath;
}
