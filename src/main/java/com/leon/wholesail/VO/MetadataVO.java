package com.leon.wholesail.VO;

import com.leon.wholesail.enums.AmountUnit;
import com.leon.wholesail.metadata.InvoiceFieldMapping;
import lombok.Data;

import java.util.List;


/**
 * <p>The input data structure to define a new mapping metadata</p>
 *
 * <ul>
 *     <li>accountingSystemName: The name of accounting system</li>
 *     <li>dateFormat: The date format that used in CSV file</li>
 *     <li>amountUnit: The amount unit that used in CSV file. Could be one of {@link AmountUnit}</li>
 *     <li>mappingFields: The detailed mapping definition of each field. {@link InvoiceFieldMapping}</li>
 * </ul>
 */
@Data
public class MetadataVO {

    private String accountingSystemName;
    private String dateFormat;
    private String amountUnit;
    private List<InvoiceFieldMapping> mappingFields;
}
