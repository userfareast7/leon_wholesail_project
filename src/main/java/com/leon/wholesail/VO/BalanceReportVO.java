package com.leon.wholesail.VO;

import com.leon.wholesail.model.BalanceReportItem;
import lombok.Data;

import java.util.LinkedList;
import java.util.List;

@Data
public class BalanceReportVO {
    public BalanceReportVO(String sellerId, String sellerName) {
        this.sellerId = sellerId;
        this.sellerName = sellerName;
        this.reportItemList = new LinkedList<>();
    }

    String sellerId;

    String sellerName;

    List<BalanceReportItem> reportItemList;
}
