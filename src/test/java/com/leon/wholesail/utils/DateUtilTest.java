package com.leon.wholesail.utils;

import org.junit.Assert;
import org.junit.Test;

import java.awt.*;
import java.time.LocalDateTime;
import java.util.Calendar;
import java.util.Date;

public class DateUtilTest {

    @Test
    public void isShouldConvertCorrectly(){
        Date date = Calendar.getInstance().getTime();
        LocalDateTime localDateTime = DateUtil.asLocalDateTime(date);
        Assert.assertEquals(localDateTime.getHour(), date.getHours());
        Assert.assertEquals(localDateTime.getMinute(), date.getMinutes());
    }
}
