package com.leon.wholesail.utils;

import com.leon.wholesail.metadata.InvoiceFieldMapping;
import com.leon.wholesail.metadata.InvoiceMapping;
import com.leon.wholesail.model.Invoice;
import com.leon.wholesail.repository.MetadataRepository;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.util.ReflectionUtils;

import java.io.IOException;
import java.lang.reflect.Method;
import java.text.ParseException;
import java.util.List;
import java.util.Map;

import static com.leon.wholesail.repository.MetadataRepository.GOLDEN_GATE_METADATA_ID;
import static com.leon.wholesail.repository.MetadataRepository.HAPPY_FRUITS_METADATA_ID;

public class InvoiceParseUtilTest {
    InvoiceParseUtil invoiceParseUtil;
    Resource goldenGateFile;

    Resource happyFruitFile;
    MetadataRepository metadataRepository;


    @Before
    public void setup() throws IOException, ParseException, NoSuchFieldException, IllegalAccessException {
        invoiceParseUtil = new InvoiceParseUtil();
        metadataRepository = MetadataRepository.getInstance();
        metadataRepository.prepareGoldenGateMapping();
        metadataRepository.prepareHappyFruitsMapping();
        goldenGateFile = new ClassPathResource("inputs/data-golden-gate-produce-10.csv");
        happyFruitFile = new ClassPathResource("inputs/data-happy-fruits-10.csv");
    }

    @Test
    public void isShouldParseCSVForGoldenGate() throws IOException, ParseException, NoSuchFieldException, IllegalAccessException {
        List<Invoice> golden_gate_invoice_list = invoiceParseUtil.parseData(goldenGateFile.getInputStream(), metadataRepository.getById(GOLDEN_GATE_METADATA_ID));
        Assert.assertEquals(10, golden_gate_invoice_list.size());
        Assert.assertTrue(golden_gate_invoice_list.get(0).getBuyer().getBuyerName().startsWith("customer-"));
        Assert.assertTrue(golden_gate_invoice_list.get(1).getBuyer().getBillingAddress().contains("Market Street, San Francisco CA 94102"));
        Assert.assertTrue(golden_gate_invoice_list.get(2).getBuyer().getShippingAddress().contains("Main Street, Oakland CA 94345"));
    }

    @Test
    public void isShouldParseCSVForHappyFruits() throws IOException, ParseException, IllegalAccessException {
        List<Invoice> happy_fruits_invoice_list = invoiceParseUtil.parseData(happyFruitFile.getInputStream(), metadataRepository.getById(HAPPY_FRUITS_METADATA_ID));
        Assert.assertEquals(10, happy_fruits_invoice_list.size());
        Assert.assertTrue(happy_fruits_invoice_list.get(0).getBuyer().getBuyerName().startsWith("buyer-"));
        Assert.assertTrue(happy_fruits_invoice_list.get(1).getBuyer().getBillingAddress().contains("Market Street, San Francisco CA 94102"));
        Assert.assertTrue(happy_fruits_invoice_list.get(2).getBuyer().getShippingAddress().contains("Main Street, Oakland CA 94345"));
    }

    @Test
    public void isShouldParseBuyerNameCorrectly() {
        Method method = ReflectionUtils.findMethod(InvoiceParseUtil.class, "setInvoiceField", Object.class, InvoiceFieldMapping.class, Object.class);
        ReflectionUtils.makeAccessible(method);
        InvoiceMapping invoiceMapping = metadataRepository.getById(GOLDEN_GATE_METADATA_ID);
        Map<String, InvoiceFieldMapping> fieldMapping = invoiceMapping.getMappingFields();
        Invoice invoice = new Invoice();
        try {
            method.invoke(invoiceParseUtil, invoice, fieldMapping.get("customer"), "customer-1");
            Assert.assertEquals("customer-1", invoice.getBuyer().getBuyerName());
        } catch (Exception e) {
            Assert.fail("Doesn't expect exception, but got exception: " + e.getMessage());
        }
    }

    @Test
    public void isShouldParseBuyerShippingAddressCorrectly() {
        Method method = ReflectionUtils.findMethod(InvoiceParseUtil.class, "setInvoiceField", Object.class, InvoiceFieldMapping.class, Object.class);
        ReflectionUtils.makeAccessible(method);
        InvoiceMapping invoiceMapping = metadataRepository.getById(GOLDEN_GATE_METADATA_ID);
        Map<String, InvoiceFieldMapping> fieldMapping = invoiceMapping.getMappingFields();
        Invoice invoice = new Invoice();
        try {
            method.invoke(invoiceParseUtil, invoice, fieldMapping.get("shipping_address"), "7 Main Street, Oakland CA 94345");
            Assert.assertEquals("7 Main Street, Oakland CA 94345", invoice.getBuyer().getShippingAddress());
        } catch (Exception e) {
            Assert.fail("Doesn't expect exception, but got exception: " + e.getMessage());
        }
    }

    @Test
    public void isShouldParseBuyerBillingAddressCorrectly() {
        Method method = ReflectionUtils.findMethod(InvoiceParseUtil.class, "setInvoiceField", Object.class, InvoiceFieldMapping.class, Object.class);
        ReflectionUtils.makeAccessible(method);
        InvoiceMapping invoiceMapping = metadataRepository.getById(GOLDEN_GATE_METADATA_ID);
        Map<String, InvoiceFieldMapping> fieldMapping = invoiceMapping.getMappingFields();
        Invoice invoice = new Invoice();
        try {
            method.invoke(invoiceParseUtil, invoice, fieldMapping.get("billing_address"), "7 Market Street, San Francisco CA 94102");
            Assert.assertEquals("7 Market Street, San Francisco CA 94102", invoice.getBuyer().getBillingAddress());
        } catch (Exception e) {
            Assert.fail("Doesn't expect exception, but got exception: " + e.getMessage());
        }
    }

}
