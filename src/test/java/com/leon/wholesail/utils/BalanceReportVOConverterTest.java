package com.leon.wholesail.utils;

import com.leon.wholesail.VO.BalanceReportVO;
import com.leon.wholesail.model.BalanceReport;
import com.leon.wholesail.model.BalanceReportItem;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.text.ParseException;
import java.util.HashMap;
import java.util.Map;

public class BalanceReportVOConverterTest {

    BalanceReport balanceReport;

    BalanceReportItem reportItem;

    @Before
    public void setup() throws IOException, ParseException, NoSuchFieldException, IllegalAccessException {

        reportItem = new BalanceReportItem("buyerId");

        balanceReport = new BalanceReport("sellerId", "sellerName");

        Map<String, BalanceReportItem> itemMap = new HashMap<>();
        itemMap.put("buyerId", reportItem);
        balanceReport.setReportItemMap(itemMap);
    }

    @Test
    public void isShouldParseCSVForGoldenGate() throws IOException, ParseException, NoSuchFieldException, IllegalAccessException {
        BalanceReportVO vo = BalanceReportVOConverter.convertToVO(balanceReport);
        Assert.assertEquals(1, vo.getReportItemList().size());
        Assert.assertEquals("sellerName", vo.getSellerName());
        Assert.assertEquals("sellerId", vo.getSellerId());
    }
}
