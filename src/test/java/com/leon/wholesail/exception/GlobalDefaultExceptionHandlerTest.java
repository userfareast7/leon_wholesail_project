package com.leon.wholesail.exception;

import com.leon.wholesail.utils.ResponseBuilder;
import com.leon.wholesail.utils.ResponseCode;
import org.junit.Test;

import java.io.IOException;
import java.util.Map;

import static com.leon.wholesail.constant.ResponseMessage.BAD_REQUEST_MESSAGE;
import static com.leon.wholesail.constant.ResponseMessage.INTERNAL_ERROR_MESSAGE;
import static org.junit.Assert.assertEquals;

public class GlobalDefaultExceptionHandlerTest {
    private GlobalDefaultExceptionHandler handler = new GlobalDefaultExceptionHandler();

    @Test
    public void handleBaseException() {
        final Map<String, Object> response = handler.handleBaseException(new IOException());
        String message = (String) response.get(ResponseBuilder.KEY_MESSAGE);
        Integer code = (Integer) response.get(ResponseBuilder.KEY_CODE);
        assertEquals(INTERNAL_ERROR_MESSAGE, message);
        assertEquals(ResponseCode.INTERNAL_ERROR.getValue(), code.intValue());
    }

    @Test
    public void handleIllegalArgumentException() {
        final Map<String, Object> response = handler.handleIllegalArgumentException(new IllegalArgumentException("Illegal input here"));
        String message = (String) response.get(ResponseBuilder.KEY_MESSAGE);
        Integer code = (Integer) response.get(ResponseBuilder.KEY_CODE);
        String detail = (String) response.get(ResponseBuilder.KEY_DETAIL);
        assertEquals(BAD_REQUEST_MESSAGE, message);
        assertEquals(ResponseCode.BAD_REQUEST.getValue(), code.intValue());
        assertEquals("Illegal input here", detail);
    }
}
