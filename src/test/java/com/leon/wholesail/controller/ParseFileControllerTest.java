package com.leon.wholesail.controller;

import com.leon.wholesail.model.BalanceReport;
import com.leon.wholesail.model.BalanceReportItem;
import com.leon.wholesail.service.BalanceReportService;
import jakarta.annotation.Resource;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.net.URL;
import java.util.HashMap;
import java.util.Map;

import static org.mockito.ArgumentMatchers.any;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.log;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest
public class ParseFileControllerTest {

    @Resource
    MockMvc mockMvc;

    @org.springframework.boot.test.mock.mockito.MockBean
    BalanceReportService balanceReportService;

    BalanceReport balanceReport;

    BalanceReportItem reportItem;

    @Before
    public void setup() {
        reportItem = new BalanceReportItem("buyerId");
        balanceReport = new BalanceReport("sellerId", "Happy Fruits");
        Map<String, BalanceReportItem> itemMap = new HashMap<>();
        itemMap.put("buyerId", reportItem);
        balanceReport.setReportItemMap(itemMap);
    }


    @Test
    public void isShouldCreateBalanceReport() throws Exception {
        Mockito.when(balanceReportService.generateBalanceReport(any(), any())).thenReturn(balanceReport);
        URL baseURI = new URL(String.format("http://%s:%d/", "127.0.0.1", 8080));
        this.mockMvc.perform(post(baseURI.toURI().resolve("balanceReport"))
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{\n" +
                                "    \"metadataId\":\"HappyFruits_MetaData_Id\",\n" +
                                "    \"filePath\":\"inputs/data-happy-fruits-10.csv\"\n" +
                                "}"))
                .andDo(log())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.data.sellerName").value("Happy Fruits"))
                .andReturn();
    }
}
