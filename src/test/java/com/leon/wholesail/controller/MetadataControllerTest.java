package com.leon.wholesail.controller;

import com.leon.wholesail.enums.AmountUnit;
import com.leon.wholesail.metadata.InvoiceMapping;
import com.leon.wholesail.metadata.InvoiceFieldMapping;
import com.leon.wholesail.service.MetadataService;
import jakarta.annotation.Resource;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.net.URL;
import java.util.HashMap;
import java.util.Map;

import static com.leon.wholesail.repository.MetadataRepository.GOLDEN_GATE_METADATA_ID;
import static org.mockito.ArgumentMatchers.any;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.log;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest
public class MetadataControllerTest {
    @Resource
    MockMvc mockMvc;

    @org.springframework.boot.test.mock.mockito.MockBean
    MetadataService metadataService;

    @org.springframework.boot.test.mock.mockito.MockBean
    ParseFileController parseFileController;

    InvoiceMapping sellerDataMapping;

    @Before
    public void setup() {
        Map<String, InvoiceFieldMapping> mappingFields = new HashMap<>();
        InvoiceFieldMapping fieldMetadata1 = new InvoiceFieldMapping();
        InvoiceFieldMapping fieldMetadata2 = new InvoiceFieldMapping();
        InvoiceFieldMapping fieldMetadata3 = new InvoiceFieldMapping();
        mappingFields.put("field1", fieldMetadata1);
        mappingFields.put("field2", fieldMetadata2);
        mappingFields.put("field3", fieldMetadata3);

        sellerDataMapping = new InvoiceMapping(GOLDEN_GATE_METADATA_ID,
                "Golden Gate",
                "yyyy-MM-dd",
                AmountUnit.DOLLAR, mappingFields);
    }


    @Test
    public void isShouldReturnMetadata() throws Exception {
        Mockito.when(metadataService.getMetadataById(any())).thenReturn(sellerDataMapping);
        URL baseURI = new URL(String.format("http://%s:%d/", "127.0.0.1", 8080));
        this.mockMvc.perform(get(baseURI.toURI().resolve("metadata/GoldenGate_MetaData_Id"))
                        .contentType(MediaType.APPLICATION_JSON))
                .andDo(log())
                .andExpect(status().isOk())
                .andReturn();
    }

    @Test
    public void isShouldCreateMetadata() throws Exception {
        Mockito.when(metadataService.createMetadata(any())).thenReturn(sellerDataMapping);
        URL baseURI = new URL(String.format("http://%s:%d/", "127.0.0.1", 8080));
        this.mockMvc.perform(post(baseURI.toURI().resolve("metadata"))
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{\n" +
                                "    \"companyName\":\"Goden Gate\",\n" +
                                "    \"dateFormat\":\"yyyy-MM-dd\",\n" +
                                "    \"amountUnit\":\"DOLLAR\",\n" +
                                "    \"mappingFields\":[\n" +
                                "        {\n" +
                                "            \"fieldName\":\"id\",\n" +
                                "            \"fieldType\":\"STRING\",\n" +
                                "            \"fieldPosition\":0,\n" +
                                "            \"targetFieldName\":\"invoiceId\"\n" +
                                "        }\n" +
                                "    ]\n" +
                                "}"))
                .andDo(log())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.data.id").exists())
                .andReturn();
    }
}
