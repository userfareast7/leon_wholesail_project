package com.leon.wholesail.repository;

import com.leon.wholesail.model.BalanceReport;
import com.leon.wholesail.model.BalanceReportItem;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

public class BalanceReportRepositoryTest {

    BalanceReportRepository balanceReportRepository;
    BalanceReport balanceReport;

    BalanceReportItem reportItem;

    @Before
    public void setup() {
        balanceReportRepository = new BalanceReportRepository();

        reportItem = new BalanceReportItem("buyerId");

        balanceReport = new BalanceReport("sellerId", "sellerName");

        Map<String, BalanceReportItem> itemMap = new HashMap<>();
        itemMap.put("buyerId", reportItem);
        balanceReport.setReportItemMap(itemMap);
    }

    @Test
    public void isShouldReturnWantedReport() {
        BalanceReport report = balanceReportRepository.createBalanceReport("sellerId", "sellerName");
        Assert.assertEquals("sellerId", report.getSellerId());
        Assert.assertEquals("sellerName", report.getSellerName());
    }

    @Test
    public void isShouldSavedReportItem() {
        balanceReportRepository.addReportItemToReport(balanceReport, "buyerId", reportItem);
        Assert.assertEquals(1, balanceReport.getReportItemMap().size());
    }

    @Test
    public void isShouldGetReportItem() {
        Optional<BalanceReportItem> itemOptional = balanceReportRepository.getReportItemFromReport(balanceReport, "buyerId");
        Assert.assertTrue(itemOptional.isPresent());
        Assert.assertEquals("buyerId", itemOptional.get().getCustomer());
    }
}
