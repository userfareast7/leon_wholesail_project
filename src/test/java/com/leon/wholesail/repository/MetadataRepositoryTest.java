package com.leon.wholesail.repository;

import com.leon.wholesail.enums.AmountUnit;
import com.leon.wholesail.metadata.InvoiceMapping;
import com.leon.wholesail.metadata.InvoiceFieldMapping;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

import static com.leon.wholesail.repository.MetadataRepository.GOLDEN_GATE_METADATA_ID;

public class MetadataRepositoryTest {

    MetadataRepository metadataRepository;

    InvoiceMapping sellerDataMapping;

    @Before
    public void setup() {
        metadataRepository = MetadataRepository.getInstance();
        metadataRepository.prepareGoldenGateMapping();
        metadataRepository.prepareHappyFruitsMapping();

        Map<String, InvoiceFieldMapping> mappingFields = new HashMap<>();
        InvoiceFieldMapping fieldMetadata1 = new InvoiceFieldMapping();
        InvoiceFieldMapping fieldMetadata2 = new InvoiceFieldMapping();
        InvoiceFieldMapping fieldMetadata3 = new InvoiceFieldMapping();
        mappingFields.put("field1", fieldMetadata1);
        mappingFields.put("field2", fieldMetadata2);
        mappingFields.put("field3", fieldMetadata3);

        sellerDataMapping = new InvoiceMapping(GOLDEN_GATE_METADATA_ID,
                "Golden Gate",
                "yyyy-MM-dd",
                AmountUnit.DOLLAR, mappingFields);
    }

    @Test
    public void isShouldReturnMetaWithGivenId() {
        sellerDataMapping.setId(GOLDEN_GATE_METADATA_ID);
        metadataRepository.save(sellerDataMapping);
        Assert.assertEquals(3, metadataRepository.getById(GOLDEN_GATE_METADATA_ID).getMappingFields().size());
    }

    @Test
    public void isShouldReturnMetaWithGeneratedUUID() {
        InvoiceMapping createdMeta = metadataRepository.save(sellerDataMapping);
        Assert.assertTrue(createdMeta.getId() != null);
    }
}
