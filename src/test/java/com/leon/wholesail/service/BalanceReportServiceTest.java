package com.leon.wholesail.service;

import com.leon.wholesail.enums.NoteType;
import com.leon.wholesail.model.BalanceReport;
import com.leon.wholesail.model.BalanceReportItem;
import com.leon.wholesail.model.Invoice;
import com.leon.wholesail.repository.BalanceReportRepository;
import com.leon.wholesail.repository.MetadataRepository;
import com.leon.wholesail.service.impl.BalanceReportServiceImpl;
import com.leon.wholesail.utils.InvoiceParseUtil;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;

import java.io.IOException;
import java.math.BigDecimal;
import java.text.ParseException;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import static com.leon.wholesail.repository.MetadataRepository.GOLDEN_GATE_METADATA_ID;
import static com.leon.wholesail.repository.MetadataRepository.HAPPY_FRUITS_METADATA_ID;

public class BalanceReportServiceTest {
    BalanceReportServiceImpl balanceReportService;

    BalanceReportRepository balanceReportRepository;
    Resource goldenGateFile;

    Resource happyFruitFile;

    InvoiceParseUtil invoiceParseUtil;

    MetadataRepository metadataRepository;

    List<Invoice> golden_gate_invoice_list;
    List<Invoice> happy_fruits_invoice_list;

    @Before
    public void setup() throws IOException, ParseException, NoSuchFieldException, IllegalAccessException {
        invoiceParseUtil = new InvoiceParseUtil();
        balanceReportRepository = new BalanceReportRepository();

        metadataRepository = MetadataRepository.getInstance();
        metadataRepository.prepareGoldenGateMapping();
        metadataRepository.prepareHappyFruitsMapping();


        balanceReportService = new BalanceReportServiceImpl();
        balanceReportService.setBalanceReportRepository(balanceReportRepository);


        goldenGateFile = new ClassPathResource("inputs/data-golden-gate-produce-10.csv");
        golden_gate_invoice_list = invoiceParseUtil.parseData(goldenGateFile.getInputStream(), metadataRepository.getById(GOLDEN_GATE_METADATA_ID));

        happyFruitFile = new ClassPathResource("inputs/data-happy-fruits-10.csv");
        happy_fruits_invoice_list = invoiceParseUtil.parseData(happyFruitFile.getInputStream(), metadataRepository.getById(HAPPY_FRUITS_METADATA_ID));
    }

    @Test
    public void isShouldGenerateBalanceReport_GoldenGate() throws IOException, ParseException, NoSuchFieldException, IllegalAccessException {
        BalanceReport balanceReport = balanceReportService.generateBalanceReport(GOLDEN_GATE_METADATA_ID, "GOLDEN GATE", golden_gate_invoice_list);

        BalanceReportItem item = balanceReport.getReportItemMap().get("customer-7");
        Assert.assertEquals("425", item.getOutstandingBalance().toString());
        Assert.assertEquals("50", item.getPastDueBalance().toString());
    }

    @Test
    public void isShouldGenerateBalanceReport_HappyFruits() throws IOException, ParseException, NoSuchFieldException, IllegalAccessException {
        BalanceReport balanceReport = balanceReportService.generateBalanceReport(HAPPY_FRUITS_METADATA_ID, "HAPPY FRUITS", happy_fruits_invoice_list);

        BalanceReportItem item = balanceReport.getReportItemMap().get("buyer-7");
        Assert.assertEquals("850.73", item.getOutstandingBalance().toString());
        Assert.assertEquals("850.73", item.getPastDueBalance().toString());
    }

    @Test
    public void isShouldCalculateOutStandingCorrectly() {
        BalanceReportItem item = new BalanceReportItem("");
        Invoice invoice = new Invoice();
        invoice.setTransactionAmount(new BigDecimal("10.5"));
        invoice.setAdjustmentAmount(new BigDecimal("2.5"));
        invoice.setPaidAmount(new BigDecimal("5"));
        balanceReportService.calculateOutstandingBalance(invoice, item);
        Assert.assertEquals("8.0", item.getOutstandingBalance().toString());
    }


    @Test
    public void isShouldNotCalculateOutstandingBalance() {
        BalanceReportItem item = new BalanceReportItem("");
        Invoice invoice = new Invoice();
        invoice.setTransactionAmount(new BigDecimal("10.5"));
        invoice.setAdjustmentAmount(new BigDecimal("-12.5"));
        invoice.setPaidAmount(new BigDecimal("5"));
        balanceReportService.calculateOutstandingBalance(invoice, item);
        Assert.assertEquals("0", item.getOutstandingBalance().toString());
    }

    @Test
    public void isShouldCalculateWithNegativeAdjustment() {
        BalanceReportItem item = new BalanceReportItem("");
        Invoice invoice = new Invoice();
        invoice.setTransactionAmount(new BigDecimal("10.5"));
        invoice.setAdjustmentAmount(new BigDecimal("-2.5"));
        invoice.setPaidAmount(new BigDecimal("5"));
        balanceReportService.calculateOutstandingBalance(invoice, item);
        Assert.assertEquals("3.0", item.getOutstandingBalance().toString());
    }

    @Test
    public void isShouldCalculatePastDueWithNegativeAdjustment() {
        BalanceReportItem item = new BalanceReportItem("");
        Invoice invoice = new Invoice();
        invoice.setPaymentTerm(10);
        invoice.setCreatedDate(new Date());
        invoice.setTransactionAmount(new BigDecimal("10.5"));
        invoice.setAdjustmentAmount(new BigDecimal("-2.5"));
        invoice.setPaidAmount(new BigDecimal("5"));
        balanceReportService.calculatePastDueBalance(invoice, item);
        Assert.assertEquals("0", item.getOutstandingBalance().toString());
    }

    @Test
    public void isShouldGetPositivePastDueCorrectly() {
        BalanceReportItem item = new BalanceReportItem("");

        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.YEAR, 2022);
        calendar.set(Calendar.MONTH, 2);
        calendar.set(Calendar.DAY_OF_MONTH, 21);
        Date date = calendar.getTime();

        Invoice invoice = new Invoice();
        invoice.setPaymentTerm(9);
        invoice.setCreatedDate(date);
        invoice.setTransactionAmount(new BigDecimal("10.5"));
        invoice.setAdjustmentAmount(new BigDecimal("0.5"));
        invoice.setPaidAmount(new BigDecimal("5"));
        balanceReportService.calculatePastDueBalance(invoice, item);
        Assert.assertEquals("6.0", item.getPastDueBalance().toString());
    }

    @Test
    public void isShouldGetZeroPastDueCorrectly() {
        BalanceReportItem item = new BalanceReportItem("");

        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.YEAR, 2022);
        calendar.set(Calendar.MONTH, 2);
        calendar.set(Calendar.DAY_OF_MONTH, 21);
        Date date = calendar.getTime();

        Invoice invoice = new Invoice();
        invoice.setPaymentTerm(11);
        invoice.setCreatedDate(date);
        invoice.setTransactionAmount(new BigDecimal("10.5"));
        invoice.setAdjustmentAmount(new BigDecimal("0.5"));
        invoice.setPaidAmount(new BigDecimal("5"));
        balanceReportService.calculatePastDueBalance(invoice, item);
        Assert.assertEquals("0", item.getPastDueBalance().toString());
    }

    @Test
    public void isShouldNotCalculateOutStanding_VOIDED() {
        BalanceReportItem item = new BalanceReportItem("");
        Invoice invoice = new Invoice();
        invoice.setNote(NoteType.VOIDED.name());
        invoice.setTransactionAmount(new BigDecimal("10.5"));
        invoice.setAdjustmentAmount(new BigDecimal("2.5"));
        invoice.setPaidAmount(new BigDecimal("5"));
        balanceReportService.calculateOutstandingBalance(invoice, item);
        Assert.assertEquals("0", item.getOutstandingBalance().toString());
    }

    @Test
    public void isShouldNotCalculatePastDue_VOIDED() {
        BalanceReportItem item = new BalanceReportItem("");
        Invoice invoice = new Invoice();
        invoice.setNote(NoteType.VOIDED.name());
        invoice.setTransactionAmount(new BigDecimal("10.5"));
        invoice.setAdjustmentAmount(new BigDecimal("2.5"));
        invoice.setPaidAmount(new BigDecimal("5"));
        balanceReportService.calculatePastDueBalance(invoice, item);
        Assert.assertEquals("0", item.getOutstandingBalance().toString());
    }
}
